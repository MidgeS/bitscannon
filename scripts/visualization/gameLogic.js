define(['phaser',
        './preload',
        './create',
        './spriteFactory',
        './bits/bitFactory',
        './targets/targetFactory',
        './scene'],
        function(phaser,preloader,createGame,spriteFactory,bitFactory,targetFactory,scene){
    var initialize = function(){
        this.game = new Phaser.Game(800,600,Phaser.AUTO, 'gamefield', {preload: preload, create: create},true)
    }

    function preload(){
        preloader(this.game)
    }

    function create(){

        //OPTIMIZE: manage factories?
        spriteFactory.initialize(this.game)
        bitFactory.initialize(this.game)
        targetFactory.initialize(this.game)

        createGame(this.game)

        scene.initialize()
        this.game.input.activePointer.leftButton.onDown.add(function(){
            var bit = bitFactory.createBit("kreygasm",1,"kreygasm")
            scene.shoot(bit)
        })
        this.game.input.activePointer.middleButton.onDown.add(function(){
            var bit = bitFactory.createBit('kappa1000',1000,"kappa")
            scene.shoot(bit)
        })
        this.game.input.activePointer.rightButton.onDown.add(function(){
            var bit = bitFactory.createBit('kreygasm10000',10000,"kreygasm")
            scene.shoot(bit)
        })
    }
    /*
    function update(){
        updateCycle()
    }
    */

    var putMessage = function(cheers,message){
        var bits = []
        for(var i = 0; i<cheers.length; i++){
            bits.push(bitFactory.createBit(cheers[i].type,cheers[i].value,cheers[i].group));
        }
        scene.shoot(bits)
    }

    return {
        initialize: initialize,
        putMessage: putMessage
    }
})