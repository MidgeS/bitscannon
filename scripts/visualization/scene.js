define(['./cannon/cannonFactory',
        './targets/targetFactory'],
        function(cannonFactory, targetFactory){
    var initialize = function(){
        this.cannon = cannonFactory.createCannon("cannon",0,550,90,-40,45)
        this.target = targetFactory.createTarget(750,530,'wall_dummy',1000,40,100)
        this.cannonQueue = []
        this.shootInterval = null
    }

    var shoot = function(bits){
        this.cannonQueue = this.cannonQueue.concat(bits)
        var scene = this
        if(this.shootInterval == undefined){
            console.log("create new interval")
            this.shootInterval = setInterval(function(){
                console.log("fire")
                scene.cannon.shoot(scene.cannonQueue[0].sprite.body)
                scene.cannonQueue.splice(0,1)
                if(scene.cannonQueue.length === 0){
                    console.log("clear cannon")
                    clearInterval(scene.shootInterval)
                    scene.shootInterval = null
                }
            },150)
            console.log(this.shootInterval)
        } else {
            console.log("interval exists")
            console.log(this.shootInterval)
        }
    }



    return {
       initialize: initialize,
       shoot: shoot
    }
})