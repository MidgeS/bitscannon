define(['./gameLogic'],function(gameLogic){
    var initialize = function(game){
        this.game = game
    }

    var createSprite = function(x,y,name){
        var sprite = this.game.add.sprite(x,y,name)
        return  sprite
    }

    return {
        initialize: initialize,
        createSprite: createSprite
    }
})