define(['./spriteFactory'], function (spriteFactory){

    return function (game,cannon) {

        game.physics.startSystem(Phaser.Physics.P2JS)
        game.physics.p2.gravity.y = 100
        game.physics.p2.restitution = 0.8
        game.physics.p2.friction = 100

        game.physics.p2.setImpactEvents(true);

        game.bitCollisionGroup = game.physics.p2.createCollisionGroup()
        game.targetCollisionGroup = game.physics.p2.createCollisionGroup()
        game.levelCollisionGroup = game.physics.p2.createCollisionGroup()
        
        game.physics.p2.updateBoundsCollisionGroup();

        var ground = spriteFactory.createSprite(400,590,'ground_dummy')
        game.physics.p2.enable(ground)
        ground.body.static = true
        ground.body.setCollisionGroup(game.bitCollisionGroup)
        ground.body.collides(game.bitCollisionGroup)
    }
});