define(function (){
    return function (game) {
        game.stage.disableVisibilityChange = true;

        //OPTIMIZE: make ressouce loading more modular/dynamic
        game.load.image('cannon','assets/cannon/cannon-with-cannon-balls.jpg')

        //Kreygasms
        game.load.image('kreygasm','assets/cheers/kreygasm/kreygasm.png')
        game.load.image('kreygasm100','assets/cheers/kreygasm/kreygasm100.png')
        game.load.image('kreygasm1000','assets/cheers/kreygasm/kreygasm1000.png')
        game.load.image('kreygasm5000','assets/cheers/kreygasm/kreygasm5000.png')
        game.load.image('kreygasm10000','assets/cheers/kreygasm/kreygasm10000.png')

        game.load.physics('kreygasm_physics','assets/cheers/kreygasm/kreygasm.json')

        //Muxy
        game.load.image('muxy','assets/cheers/muxy/muxy.png')
        game.load.image('muxy100','assets/cheers/muxy/muxy100.png')
        game.load.image('muxy1000','assets/cheers/muxy/muxy1000.png')
        game.load.image('muxy5000','assets/cheers/muxy/muxy5000.png')
        game.load.image('muxy10000','assets/cheers/muxy/muxy10000.png')

        game.load.physics('muxy_physics','assets/cheers/muxy/muxy.json')

        //streamlabs
        game.load.image('streamlabs','assets/cheers/streamlabs/streamlabs.png')
        game.load.image('streamlabs100','assets/cheers/streamlabs/streamlabs100.png')
        game.load.image('streamlabs1000','assets/cheers/streamlabs/streamlabs1000.png')
        game.load.image('streamlabs5000','assets/cheers/streamlabs/streamlabs5000.png')
        game.load.image('streamlabs10000','assets/cheers/streamlabs/streamlabs10000.png')

        game.load.physics('streamlabs_physics','assets/cheers/streamlabs/streamlabs.json')

        //swiftrage
        game.load.image('swiftrage','assets/cheers/swiftrage/swiftrage.png')
        game.load.image('swiftrage100','assets/cheers/swiftrage/swiftrage100.png')
        game.load.image('swiftrage1000','assets/cheers/swiftrage/swiftrage1000.png')
        game.load.image('swiftrage5000','assets/cheers/swiftrage/swiftrage5000.png')
        game.load.image('swiftrage10000','assets/cheers/swiftrage/swiftrage10000.png')

        game.load.physics('swiftrage_physics','assets/cheers/swiftrage/swiftrage.json')

        //cheer
        game.load.image('cheer','assets/cheers/cheer/cheer.png')
        game.load.image('cheer100','assets/cheers/cheer/cheer100.png')
        game.load.image('cheer1000','assets/cheers/cheer/cheer1000.png')
        game.load.image('cheer5000','assets/cheers/cheer/cheer5000.png')
        game.load.image('cheer10000','assets/cheers/cheer/cheer10000.png')

        game.load.physics('cheer_physics','assets/cheers/cheer/cheer.json')

        //Kappas
        game.load.image('kappa','assets/cheers/kappa/kappa.png')
        game.load.image('kappa100','assets/cheers/kappa/kappa100.png')
        game.load.image('kappa1000','assets/cheers/kappa/kappa1000.png')
        game.load.image('kappa5000','assets/cheers/kappa/kappa5000.png')
        game.load.image('kappa10000','assets/cheers/kappa/kappa10000.png')

        game.load.physics('kappa_physics','assets/cheers/kappa/kappa.json')


        game.load.image('ground_dummy','assets/ground/ground_dummy.png')
        game.load.image('wall_dummy','assets/targets/wall_dummy.jpg')
    }
});