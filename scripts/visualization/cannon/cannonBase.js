define(['../spriteFactory'], function (spriteFactory) {

    function cannon(image,x,y,muzzleX,muzzleY,angle){
        this.image = image
        this.x = x
        this.y = y
        this.muzzleOffset = {
            x: muzzleX,
            y: muzzleY
        }
        this.angle = angle
        this.power = 300

        //create sprite
        this.sprite = spriteFactory.createSprite(this.x,this.y,this.image)
        this.sprite.height = 100
        this.sprite.width = 100
        this.sprite.bottom = 600
    }

    cannon.prototype.getPower= function() {
        return this.power
    }
    cannon.prototype.shoot = function (bit){
        bit.sprite.reset(this.getMuzzlePosition().x,this.getMuzzlePosition().y)
        bit.sprite.body.angle = this.getShotAngle()
        bit.sprite.body.moveForward(this.getPower())
    }
    cannon.prototype.getMuzzlePosition = function (){
        return {
            x: this.x + this.muzzleOffset.x, 
            y: this.y + this.muzzleOffset.y, 
        }
    }
    cannon.prototype.getShotAngle = function (){
        var angle = Math.random() * (90 - 35 + 1) + 35
        return angle
    }

    return cannon
});