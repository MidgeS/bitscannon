define(['./cannonBase'],function(cannonBase){
    var createCannon = function(image,x,y,muzzleX,muzzleY,angle){
        //OPTIMIZE: cache cannons / cannon pooling
        var Cannon = new cannonBase(image,x,y,muzzleX,muzzleY,angle)
        return Cannon
    }

    return {
        createCannon: createCannon
    }
})