define(['./gameLogic','./cheerParser','../twitch_connection/PubSubConnection',
        '../twitch_connection/twitchConnect'],
        function(gameLogic, cheerParser,PubSubConnection,TwitchConnect){
   
    console.log("stuff starts here")
    if(TwitchConnect.checkConnection()){
        gameLogic.initialize()   
        cheerParser.initialize() 
        pub_sub = new PubSubConnection()
        pub_sub.addStateChangeCallback(handleStateChange)
        pub_sub.addBitMessageCallback(handleBitMessage)
        pub_sub.connect()

        function handleBitMessage(message){
            var result = cheerParser.parse(message)
            gameLogic.putMessage(result,message)
        }

        function handleStateChange(state){
            console.log("new state: "+state)
        }
    } else {
        console.log("not connected")
    }

    /**setTimeout(function(){
        var message = "Hello sinq cheer1 cheer10 cheer100 cheer1000 cheer5000 cheer10000"
        var result = cheerParser.parse(message) 
        gameLogic.putMessage(result,message)
    },10000) **/
});