define(['../spriteFactory'], function (spriteFactory){
    var target = function(x,y,image,health,width,heigth){
        this.image = image
        this.x = x
        this.y = y
        this.health = health
        this.width = width
        this.heigth = heigth
        this.sprite = ""

        this.sprite = spriteFactory.createSprite(this.x,this.y,this.image)
    }

    target.prototype.takeDamage = function(damage){
        this.health -= damage
        if(this.health <= 0){
            this.sprite.kill()
        } else {
            //console.log("health left: "+this.health)
        }
    }

    target.prototype.colent = function(body1){
        //console.log(arguments)
        //console.log("test")
    }

    target.prototype.gotHit = function(target,bit){
        //console.log("ouch!")
        //TODO: create proper calculation
        var x = bit.velocity.x
        var y = bit.velocity.y
        var speed = Math.sqrt((x*x)+(y*y))
        var damage = Math.log(bit.mass * speed)
        /**console.log("mass: "+bit.mass)
        console.log("speed: "+speed)
        console.log("damage: "+damage)**/
        this.takeDamage(damage)
    }

    return target
})