define(['./targetBase'],function(targetBase){
    var initialize = function(game){
        this.game = game
    }

    var createTarget = function(x,y,type,health,width,height){
        var target = new targetBase(x,y,type,health,width,height)

        target.sprite.height = height
        target.sprite.width = width
        this.game.physics.p2.enable(target.sprite)
        target.sprite.body.static = true
        target.sprite.body.setCollisionGroup(this.game.targetCollisionGroup)
        target.sprite.body.collides(this.game.bitCollisionGroup, target.gotHit,target)
        target.sprite.body.onBeginContact.add(target.colent, target)
    

        return target
    }

    return {
        initialize: initialize,
        createTarget: createTarget
    }
})