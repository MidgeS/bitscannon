define(['./bits/cheerList'], function(cheerList){

    var initialize = function(){
        var cheers = cheerList.getList()
        this.regString = ''
        for(var i = 0; i<cheers.cheerTypes.length; i++){
            this.regString = this.regString + "("+cheers.cheerTypes[i]+"\\d+)"
            if(i < (cheers.cheerTypes.length -1)){
                this.regString = this.regString + "|"
            }
        }
        this.bitReg = new RegExp(this.regString,"gim")
    }

    var findRange = function(value){
        var ranges = cheerList.getList().valueRanges
        for(var i = ranges.length-1; i>= 0; i--){
            if(value >= ranges[i]){
                return (i === 0) ? '' : ranges[i]
            }
        }
    }

    var parse = function(message){
        var result = message.match(this.bitReg)

        var cheers = []
        for(var i=0; i<result.length; i++){
            var data = result[i].split(/(\d+)/)
            var range = this.findRange(data[1])
            cheers[i] = {
                //TODO: determine exact emote
                type: data[0].toLowerCase()+range,
                value: data[1],
                group: data[0].toLowerCase()
            }
        }

        return cheers

        //NOTE: possible to get positions for text display
        /*
        var result2 = this.bitReg.exec(text)
        console.log(result2)
        result2 = this.bitReg.exec(text)
        console.log(result2)*/
    }

    return {
        initialize: initialize,
        parse: parse,
        findRange: findRange
    }
})