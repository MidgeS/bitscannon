define(['./bitBase'],function(bitBase){
    var initialize = function(game){
        this.game = game
    }

    var createBit = function(type,value,group){

        //OPTIMIZE: pool bits by type
        var bit = new bitBase(type,value)

        this.game.physics.p2.enable(bit.sprite)
        bit.sprite.checkWorldBounds = true;
        bit.sprite.outOfBoundsKill = true
        bit.sprite.events.onOutOfBounds.add(OOB,this)
        //bit.sprite.body.setCircle(12)
        bit.sprite.body.clearShapes()
        bit.sprite.body.loadPolygon(group+'_physics',type)
        bit.sprite.body.mass = value
        bit.sprite.body.setCollisionGroup(this.game.bitCollisionGroup)
        bit.sprite.body.collides([this.game.levelCollisionGroup,
                                  this.game.bitCollisionGroup,
                                  this.game.targetCollisionGroup])
        bit.sprite.body.collideWorldBounds = false
        bit.sprite.kill()
        return bit
    }

    function OOB(sprite){
        //OPTIMIZE: pool instead
        sprite.destroy()
    }

    return {
        initialize: initialize,
        createBit: createBit
    }
})