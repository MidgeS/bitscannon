define(['../spriteFactory'],function(spriteFactory){
    var bit = function(type,value){
        this.type = type
        this.value = value

        this.sprite = spriteFactory.createSprite(0,0,this.type)
    }


    return bit
})