define(function(){

    var cheers = function(){
        this.cheers = [
            "cheer", "cheer100", "cheer1000", "cheer5000", "cheer10000",
            "kappa", "kappa100", "kappa1000", "kappa5000", "kappa10000",
            "kreygasm", "kreygasm100", "kreygasm1000", "kreygasm5000", "kreygasm10000",
            "muxy", "muxy100", "muxy1000", "muxy5000", "muxy10000",
            "streamlabs", "streamlabs100", "streamlabs1000", "streamlabs5000", "streamlabs10000",
            "swiftrage", "swiftrage100", "swiftrage1000", "swiftrage5000", "swiftrage10000",
        ]
        this.cheerTypes = ["cheer", "kappa", "kreygasm", "muxy", "streamlabs", "swiftrage"],
        this.valueRanges = [1,100,1000,5000,10000]
    }

    var getList = function(){
        if(!this.cheerList){
            this.cheerList = new cheers()
        }
        return this.cheerList
    }

    return {
        getList
    }
})