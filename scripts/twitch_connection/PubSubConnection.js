define(['./twitchConnect','./bitsRequest','./PubSubStates'],function(TwitchConnect,bitsRequest,PubSubStates){

    var PubSubConnection = function(){
        this.bitMessageCallbacks = []
        this.stateChangeCallbacks = []
        this.state = PubSubStates.CLOSED
        this.addStateChangeCallback(this.handleStateChange)
    }

    PubSubConnection.prototype.addBitMessageCallback = function(callback){
        this.bitMessageCallbacks.push(callback)
    }
    PubSubConnection.prototype.addStateChangeCallback = function(callback){
        this.stateChangeCallbacks.push(callback)
    }

    PubSubConnection.prototype.fireStateChange = function(value){
        this.stateChangeCallbacks.forEach(function(element){
            element(this.state,this,value)
        },this)
    }
    PubSubConnection.prototype.fireBitMessage = function(message){
        this.bitMessageCallbacks.forEach(function(element) {
            element(message)
        }, this);
    }

    PubSubConnection.prototype.handleStateChange = function(state,self,value){
        switch(state){
            case PubSubStates.RECONNECT:
                //TODO: close connection and reopen after 30s
                break;
            case PubSubStates.CONNECTING:
                //do nothing
                break;
            case PubSubStates.CLOSED:
                //reconnect if not closed itself
                break;
            case PubSubStates.CONNECTED:
                //enable massage stuff
                break;
            case PubSubStates.ERROR:
                //handle errors
                break;
            case PubSubStates.RESPONSE_ERROR:
                console.log("relisten to bits!:"+value)
                if(value === "ERR_BADAUTHiua"){
                    setTimeout(function(){
                        console.log(self)
                        self.connectToBitsTopic()
                    },500)
                }
                break;
        }
    }

    PubSubConnection.prototype.connect = function(){
        this.state = PubSubStates.CONNECTING
        this.fireStateChange()

        this.connection = new WebSocket('wss://pubsub-edge.twitch.tv')

        console.log("connecting to pubsub")

        var self = this
        this.connection.onopen = function(message){
            self.onOpen(message)
        }
        this.connection.onerror = function(error){
            self.onError(error)
        }
        this.connection.onmessage = function(message){
            self.onMessage(message)
        }
        this.connection.onclose = function(){
            self.onClose()
        }
    }

    PubSubConnection.prototype.connectToBitsTopic = function(){
        var self = this
        //promise since id is catched async
        TwitchConnect.getChannelId().then(
        function(id){
            console.log("id: "+id)
            console.log("auth: "+TwitchConnect.getToken())
            var request = new bitsRequest(id,TwitchConnect.getToken())
            self.connection.send(JSON.stringify(request))
        })
    }

    PubSubConnection.prototype.onOpen = function(open){
        console.log("connection opened")

        var self = this

        //Ping server every 2 minutes (120 000 ms)
        this.pingInterval = setInterval(function(){
            var Ping = {type: "PING"}
            self.connection.send(JSON.stringify(Ping));
        },240000)


        //connect to bits topic
        self.connectToBitsTopic()

        this.state = PubSubStates.CONNECTED
        this.fireStateChange()


        //FIXME: remove me
        //test callback after connect
        setTimeout(function(){
            self.fireBitMessage("Hello sinq cheer1 cheer10 cheer100 cheer1000 cheer5000 cheer10000")
        },10000)
    }

    PubSubConnection.prototype.onError = function(error){
        console.log("WebSocket Error"+error)
        this.state = PubSubStates.ERROR
        this.fireStateChange()
    }

    PubSubConnection.prototype.onMessage = function(message){
        var response = JSON.parse(message.data)
        if(response.type === "PONG"){
        }
        if(response.type === "RESPONSE"){
            //TODO: handele response with nonce
            if(response.error === ""){
                console.log("listening to bits")
            } else {
                console.log("Error on request:")
                console.log(response.error) //TODO: hande errors
                this.state = PubSubStates.RESPONSE_ERROR
                this.fireStateChange(response.error)
            }
        }
        if(response.type === "MESSAGE"){
            console.log("got a message")
            console.log("topic"+response.data.topic)
            //TODO: handle type and content(json string)
            if(responce.data.topic.match(/channel-bits-events-v1/)){
                console.log("got bits!!!")
                var bitsMessage = JSON.parse(response.data.message)
                console.log(bitsMessage)
                this.fireBitMessage(bitsMessage.data.chat_message)
            }
        }
    }

    PubSubConnection.prototype.onClose = function(){
        clearInterval(this.pingInterval)
        console.log("connection closed: ")
        this.state = PubSubStates.CLOSED
        this.fireStateChange()
    }

    return PubSubConnection
})