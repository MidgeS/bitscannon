define(function(){

    var checkConnection = function(){
        //console.log("checking connection")
        if(typeof(Storage) !== "undefined"){
            if(sessionStorage.getItem("token")){
                return true
            } else {
                var hash = window.location.hash
                var token = hash.match(/access_token=\w+/)
                if(token){
                    sessionStorage.setItem("token",token[0].slice(13))
                    return true
                } else {
                    var redirect_URL = "https://api.twitch.tv/kraken/oauth2/authorize?response_type=token"
                    +"&client_id=r3kxt68clj4wgd8f0l5hjxm2b82128"
                    +"&redirect_uri=http://localhost:3000"
                    +"&state=uiaest"
                    +"&force_verify=true"
                    console.log("no valid token")
                    window.location.href = redirect_URL
                }
            }
        } else {
            return false
        }
    }

    var getToken = function(){
        return sessionStorage.getItem("token")
    }

    var getChannelId = function(){
        return new Promise(
        function(resolve,reject){
            console.log("grab id")
            var request = new XMLHttpRequest();
            request.onreadystatechange = function() {
                if (request.readyState === 4) {
                    if (request.status === 200) {
                        var id = JSON.parse(request.response).token.user_id
                        console.log("got id")
                        resolve(id)
                    } else {
                        console.log("error")
                    }
                }
            }
            var requestURL = "https://api.twitch.tv/kraken"
            request.open("GET", requestURL , true)
            request.setRequestHeader("Client-ID","r3kxt68clj4wgd8f0l5hjxm2b82128")
            request.setRequestHeader("Authorization","OAuth "+getToken())
            request.setRequestHeader("Accept","application/vnd.twitchtv.v5+json")
            request.send(null)
        })
    }

    return {
        checkConnection: checkConnection,
        getToken: getToken,
        getChannelId: getChannelId
    }
})