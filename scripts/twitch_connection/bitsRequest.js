define(function(){

    var bitsRequest = function(channel_id,token){
        this.type = "LISTEN"
        this.nonce = "dotrniaotrniaeod"
        this.data = {
            topics: ["channel-bits-events-v1."+channel_id],
            auth_token: token
        }
    }

    return bitsRequest
})