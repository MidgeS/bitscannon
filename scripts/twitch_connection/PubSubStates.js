define(function(){
    return {
        CLOSED: "closed",
        CONNECTED: "connected",
        ERROR: "error",
        RESPONSE_ERROR: "response_error",
        CONNECTING: "connecting",
        RECONNECTING: "reconnecting"
    }
})