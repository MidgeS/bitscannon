requirejs.config({
    paths: {
        text: "../../node_modules/requirejs-text/text",
        Handlebars: "../../node_modules/handlebars/dist/handlebars",
        Backbone: "../../node_modules/backbone/backbone",
        underscore: "../../node_modules/backbone/node_modules/underscore/underscore",
        jquery: "../../node_modules/jquery/dist/jquery"
    },

    shim: {
        "backbone":{
            deps:["underscore","jquery"],
            exports:"Backbone"
        },
        "underscore":{
            exports:"_"
        }
    }
})

require(['./scripts/converter'], function(){
})