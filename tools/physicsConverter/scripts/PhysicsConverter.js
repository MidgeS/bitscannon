define(['./PhaserPhysicsData','./PhaserPhysicsObject'],function(PhaserPhysicsData,PhaserPhysicsObject){

    var Converter = function(PhysicsEditorData){
        var converted = new PhaserPhysicsData
        
        PhysicsEditorData.models.forEach(function(element) {
            var data = element.attributes.data
            var newentry = new PhaserPhysicsObject
            newentry.setData(data.name)
            newentry.addShapes(data.polygons)
            converted.add(newentry)
        }, this);
        
        return converted
    }

    return Converter
})