define(['Backbone','./PhaserPhysicsObject'], function(Backbone,PhaserPhysicsObject){

    var PhaserPhysicsData = Backbone.Collection.extend({
        model: PhaserPhysicsObject,

        formatedJSON: function(){
            var output = "{\n"
            for(var i = 0; i<this.models.length; i++){
                var seperate = i === 0 ? '' : ','
                output = output + seperate + this.models[i].formatedJSON() + "\n";
            }
            return output + "}"
        }
    })

    return PhaserPhysicsData
})