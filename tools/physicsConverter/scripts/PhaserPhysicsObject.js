define(['Backbone'],function(Backbone){

    var PhaserPhysicsObject = Backbone.Model.extend({

        defaults: function(){
            return {
                name: "phaser object",
                data: {
                    density: 2,
                    friction: 0,
                    bounce: 0,
                    filter: { categoryBits: 1, maskBits: 65535 },
                    shapes: []
                }
            }
        },

        setData: function(newData){
            this.set({name: newData})
        },

        addShapes: function(polygons){
            polygons.forEach(function(element) {
                var coords = []
                element.forEach(function(coordinate){
                    //FIXME: scale by image size
                    coords.unshift(28*(1-coordinate.y))
                    coords.unshift(28*coordinate.x)
                },this)
                this.attributes.data.shapes.push(coords)
            }, this);
        },

        formatedJSON: function(){
            var att = this.attributes
            var output = {
                [att.name]: []
            }

            att.data.shapes.forEach(function(element,index){
                var field = output[att.name]
                field.push({
                    density: att.data.density,
                    friction: att.data.friction,
                    bounce: att.data.bounce,
                    filter: att.data.filter,
                    shape: att.data.shapes[index]
                })
            },this)

            return JSON.stringify(output).replace(/(^{|}$)/gi,'');
        }

    })

    return PhaserPhysicsObject
})