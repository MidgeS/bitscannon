define(['Backbone','./PhysicsEditorObject'],function(Backbone,PhysicsEditorObject){

    var PhysicsEditorData = Backbone.Collection.extend({

        parseJsonData: function(json){
            var data = JSON.parse(json)
            data.rigidBodies.forEach(function(element) {
                var body = new PhysicsEditorObject
                body.setData(element)
                this.add(body)
            }, this);
        }
    })

    return PhysicsEditorData
})