define(['Backbone','Handlebars','text!templates/PhaserPhysicsObjectView.hbs'],
        function(Backbone,Handlebars,Template){

    var PhaserPhysicsObjectView = Backbone.View.extend({

        tagName: "div",
        template: Handlebars.compile(Template),

        initialize: function(){
            this.listenTo(this.model,"change",this.render)
            this.listenTo(this.model,"destroy",this.remove)
        },

        render: function(){
            var html = this.template(this.model.toJSON())
            this.$el.html(html)
            return this
        },

        remove: function(){
            this.model.destroy()
        }

    })

    return PhaserPhysicsObjectView

})