define(['Backbone','Handlebars',
        'text!templates/ConverterView.hbs',
        './PhaserPhysicsObjectView','./PhysicsEditorData',
        './PhysicsConverter'],
        function(Backbone,Handlebars,Template,PhaserPhysicsObjectView,
                 PhysicsEditorData,PhysicsConverter){

    var ConverterView = Backbone.View.extend({

        el: '#converter',

        events: {
            "change #fileselect": "fileChange"
        },

        initialize: function(){
            this.listenTo(this.collection,"reset",this.resetCollection)
            this.listenTo(this.collection,"add",this.addObject)
            this.listenTo(this.collection,"all",this.render)
            this.info = this.$("#info")
            this.json = this.$("#json")
        },

        render: function(){
            var template = Handlebars.compile(Template)
            this.info.html(template)

            var jsonExport = this.collection.formatedJSON()
            var blob = new Blob([jsonExport],{type: "application/json"})
            var url = URL.createObjectURL(blob)

            var a = document.createElement('a')
            a.download = this.filename
            a.href = url
            a.textContent = "Download converted json"
            this.json.append(a)
            this.json.append("<pre>"+jsonExport+"</pre>")
        },

        resetCollection: function(){
            this.collection.forEach(this.addObject,this)
        },

        addObject: function(object){
            var view = new PhaserPhysicsObjectView({model: object})
            this.$("#phaser-objects").append(view.render().el)
        },

        fileChange: function(button){
            //FIXME: find cause of event fired twice
            button.stopPropagation()

            var file = button.target.files[0]
            if(!file){
            } else {
                //FIXME: not sure if correct
                this.$("#phaser-objects").html("")
                this.json.html("")

                this.filename = file.name
                var reader = new FileReader()
                var scope = this
                reader.onload = (e) => {
                    var content = e.target.result
                    var data = new PhysicsEditorData
                    data.parseJsonData(content)
                    var converted = PhysicsConverter(data)
                    this.collection.reset(converted.models)
                }
                reader.readAsText(file)
            }
        }
        
    })

    return ConverterView

})