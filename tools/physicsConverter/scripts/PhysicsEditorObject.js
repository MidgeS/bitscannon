define(['Backbone'],function(Backbone){

    var PhysicsEditorObject = Backbone.Model.extend({

        setData(data){
            this.set({data: data})
        }
    })

    return PhysicsEditorObject
})